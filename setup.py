#!/usr/bin/env python3

from setuptools import setup

import datahammer

name = 'datahammer'
url = 'https://gitlab.com/n2vram/' + name

hammer_classifiers = [
    'Development Status :: 5 - Production/Stable',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: MIT License',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3',
    'Topic :: Text Processing :: Filters',
    'Topic :: Utilities',
    'Topic :: Software Development :: Libraries :: Python Modules',
]


setup(
    name=name,
    version=datahammer.version,
    description=datahammer.description,
    long_description=open('README.rst').read(),
    long_description_content_type='text/x-rst',
    license='MIT',
    author='NVRAM',
    author_email='nvram@users.sourceforge.net',
    url=url,
    classifiers=hammer_classifiers,
    keywords=['data', 'datasets', 'queries', 'JSON', 'resultsets', 'datahammer', 'hammer'],
    py_modules=[name],
    download_url=(url + '/archive/' + datahammer.version),
    platforms=['any'],
)
